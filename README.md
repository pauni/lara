# What is Lara
This is free/libre software licenced under the GPLv3 free software license.
Use Lara to remote control your Linux computer.

## What Lara needs
*   The following packages are required: `ssh` `playerctl` `evemu-tools`  `amixer`
*   Password-less sudo is required for: `evemu-describe` `evemu-device` `evemu-event`

    You can e.g. creating following file `/etc/sudoers.d/lara`:
    ```
    [name] [host] = (root) NOPASSWD: /bin/evemu-describe
    [name] [host] = (root) NOPASSWD: /bin/evemu-device
    [name] [host] = (root) NOPASSWD: /bin/evemu-event
    ```

## What Lara can do
Control your audio volume, media playback, keyboard (in the future: mouse, view screen) of your Linux system.

## Technical Details
Connects via SSH and uses command line tools to provide it's functionality
