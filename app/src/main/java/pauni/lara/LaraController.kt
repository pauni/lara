/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import pauni.lara.LaraController
import pauni.lara.core.LinuxMachine

/**
 * I don't quite remember what this was but it doesn't seem to be used anywhere
 */
class LaraController : Service() {
    private val binder: IBinder = LocalBinder()
    override fun onBind(intent: Intent): IBinder? {
        return binder
    }

    /**
     * Class used for the client Binder. Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods
        val service: LaraController
            get() =// Return this instance of LocalService so clients can call public methods
                this@LaraController
    }

    /**
     * Connect to the Linux machine with updated values from device-dialog.
     */
    fun connectionDetailsUpdated() {
        TODO("move ssh logic here")
        // new values are automatically retrieved from Prefs by .connect().
        // I think I don't have to call this method.
        // linuxMachine.connect();
    }
}