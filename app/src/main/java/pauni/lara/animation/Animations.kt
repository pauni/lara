/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.animation

import android.animation.*
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.view.animation.AccelerateInterpolator
import android.view.animation.OvershootInterpolator
import java.util.*

/**
 * Helper class that provides some nice animations
 */
object Animations {
    private val slidingRevealInterpolator: TimeInterpolator = OvershootInterpolator(0.9f)
    private val circularRevealInterpolator: TimeInterpolator = AccelerateInterpolator()
    private const val CIRCULAR_DURATION: Long = 250
    private const val SLIDING_DURATION: Long = 500

    // keep track of views currently being animated to cancel an animation if new one is triggered
    private val animationStage: MutableMap<View?, Animator> = HashMap()
    private fun cancelPreviousAnimation(view: View?) {
        if (animationStage.containsKey(view)) {
            Objects.requireNonNull(animationStage[view])?.cancel()
            animationStage.remove(view)
        }
    }

    /**
     * Reveals a view by with a sliding animation by animating it's parent height.
     * @param child Child to be revealed.
     */
    fun slidingReveal(child: View): Animator {
        cancelPreviousAnimation(child)
        val parent = child.parent as ViewGroup

        // measure height of parent with and without child
        child.visibility = View.GONE
        val from = parent.measuredHeight
        child.visibility = View.VISIBLE
        child.measure(-1, -1)
        val to = from + child.measuredHeight
        val animator = ValueAnimator.ofInt(from, to).setDuration(SLIDING_DURATION)
        animator.interpolator = slidingRevealInterpolator
        animator.addUpdateListener { animation: ValueAnimator ->
            // apply transformation
            parent.layoutParams.height = animation.animatedValue as Int
            parent.requestLayout()
        }
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                // remove animator from stage
                animationStage.remove(child)
            }
        })

        // put child on stage before starting animation
        animationStage[child] = animator
        animator.start()
        return animator
    }

    /**
     * Hides a view by with a sliding animation by animating it's parent height.
     * @param child Child to be hidden.
     */
    fun slidingHide(child: View): Animator {
        cancelPreviousAnimation(child)
        val parent = child.parent as ViewGroup
        val lp = child.layoutParams as MarginLayoutParams

        // measure height of parent with and without child
        val from = parent.measuredHeight
        val to = from - child.measuredHeight - lp.topMargin
        val animator = ValueAnimator.ofInt(from, to).setDuration(SLIDING_DURATION)
        animator.interpolator = slidingRevealInterpolator
        animator.addUpdateListener { animation: ValueAnimator ->
            // apply transformation
            parent.layoutParams.height = animation.animatedValue as Int
            parent.requestLayout()
        }
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                // make (now hidden) child disappear
                child.visibility = View.GONE
                // remove animator from stage
                animationStage.remove(child)
            }
        })

        // put child on stage before starting animation
        animationStage[child] = animator
        animator.start()
        return animator
    }

    fun circularReveal(child: View?, x: Int, y: Int): Animator {
        cancelPreviousAnimation(child)
        child!!.visibility = View.VISIBLE
        child.requestLayout()
        child.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)

        // get the final radius for the clipping circle
        val finalRadius = Math.hypot(child.measuredWidth.toDouble(), child.measuredHeight.toDouble()).toInt()

        // create the animator for this view (the start radius is zero)
        val animator = ViewAnimationUtils.createCircularReveal(
                child, x, y, 0f, finalRadius.toFloat()).setDuration(CIRCULAR_DURATION)
        animator.interpolator = circularRevealInterpolator
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                // remove animator from stage
                animationStage.remove(child)
            }
        })

        // put child on stage before returning animation
        animationStage[child] = animator
        return animator
    }

    fun circularHide(child: View, x: Int, y: Int): Animator {
        cancelPreviousAnimation(child)

        // get the initial radius for the clipping circle
        val startRadius = Math.hypot(child.measuredWidth.toDouble(), child.measuredHeight.toDouble()).toInt()

        // create the animation (the final radius is zero)
        val animator = ViewAnimationUtils.createCircularReveal(
                child, x, y, startRadius.toFloat(), 0f).setDuration(SLIDING_DURATION)
        animator.interpolator = circularRevealInterpolator

        // make the view invisible when the animation is done
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                child.visibility = View.GONE
                // remove animator from stage
                animationStage.remove(child)
            }
        })

        // put child on stage before starting animation
        animationStage[child] = animator
        return animator
    }

    fun rotateView(view: View?, start: Int, end: Int) {
        if (view == null) return
        cancelPreviousAnimation(view)
        val anim = ObjectAnimator.ofFloat(view, "rotation", start.toFloat(), end.toFloat())
        anim.duration = 300
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                // remove animator from stage
                animationStage.remove(view)
            }
        })
        animationStage[view] = anim
        anim.start()
    }
}