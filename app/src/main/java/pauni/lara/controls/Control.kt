/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.controls

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import pauni.lara.core.LinuxMachine

/**
 * Template for an arbitrary control (panel) put on the main layout
 * of the app. Examples are media control, keyboard control, etc.
 */
abstract class Control(protected var context: Context, protected var linuxMachine: LinuxMachine) {
    abstract fun init()
    abstract val controlView: View?
    abstract val displayName: String
    abstract fun registerListeners()
    abstract fun unregisterListeners()
    abstract val displayIcon: Drawable?
}