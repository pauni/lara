/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.controls

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import pauni.lara.R

/**
 * View of a control to be put on the main layout of the app.
 */
@SuppressLint("ViewConstructor")
class ControlView(context: Context, private val control: Control) : FrameLayout(context) {
    private val expanded = true

    private fun initViews(context: Context) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.control_template, this, true)
        val txtControlTitle : TextView = findViewById(R.id.txt_control_title)
        txtControlTitle.text = control.displayName
        (findViewById<View>(R.id.img_remoteIcon) as ImageView).setImageDrawable(control.displayIcon)
        val frmControlRoot : FrameLayout = findViewById(R.id.frm_control_root)
        frmControlRoot.addView(control.controlView)
        val imbExpand : ImageButton = findViewById(R.id.imb_expand)
        imbExpand.isClickable = false
        imbExpand.isFocusable = false
        //        imbExpand.setOnClickListener(view -> {
//            // prepare animation
//            int start = expanded ? 0 : 180;
//            int end   = expanded ? 180 : 0;
//
//            // rotate expand-icon and hide or reveal control
//            Animations.rotateView(imbExpand, start, end);
//            if (expanded) Animations.slidingHide(frmControlRoot);
//            else Animations.slidingReveal(frmControlRoot);
//            expanded = !expanded;
//        });
    }

    init {
        initViews(context)
    }
}