/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.controls.keyboard

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import pauni.lara.R

/**
 * Models a key on the UI and manages it's clicks. Notifies its [Keyboard] about state changes.
 *
 * Combo keys (e.g. SHIFT) are AUTOMATICALLY released ONLY when chars (e.g. "a") are pressed.
 * Sticky keys (long press) are NEVER AUTOMATICALLY released.
 */
class Key(context: Context, val type: KeyType) : View.OnClickListener, View.OnLongClickListener {
    var label = "ERROR" // Text to be shown at the UI key (KeyView)
    val icon: Drawable? = null // Icon to be shown at the UI key (KeyView)
    private val keyState: MutableLiveData<KeyState> by lazy { MutableLiveData<KeyState>() }
    private var sticky: Boolean = false

    fun getState(): LiveData<KeyState> {
        return this.keyState
    }

    fun setState(state: KeyState) {
        this.keyState.value = state
    }

    fun getSticky(): Boolean {
        return this.sticky
    }

    /**
     * Long click callback. Toggles sticky and pressed state.
     * @param view View that was long clicked
     */
    override fun onLongClick(view: View): Boolean {
        Log.i("Key", "onLongClick($type)")
        this.keyState.value = this.keyState.value?.toggle()
        this.sticky = !this.sticky
        return true
    }

    override fun onClick(view: View) {
        Log.i("Key", "onClick($type)")

        // update state after click
        if (this.sticky || this.isComboKey()) {
            // toggle state, update sticky status
            this.keyState.value = this.keyState.value?.toggle()
            this.sticky = false
        } else {
            // keys are always RESTING in this branch
            // press and release immediately
            this.keyState.value = KeyState.PRESSED
            Thread.sleep(50)
            this.keyState.value = KeyState.RESTING
        }
    }

    /**
     * Checks whether this key is a "combo key" which means, it's sticky
     * status will always remain true.
     * @return true, if CTRL, SHIFT, ALT, ALT_GR. false, else.
     */
    private fun isComboKey(): Boolean {
        return arrayOf(KeyType.CTRL, KeyType.SHIFT, KeyType.ALT, KeyType.ALT_GR).contains(this.type)
    }

    init {
        this.keyState.value = KeyState.RESTING
        when (type) {
            KeyType.CTRL -> label = context.resources.getString(R.string.key_CTRL)
            KeyType.SHIFT -> label = context.resources.getString(R.string.key_SHIFT)
            KeyType.TAB -> label = context.resources.getString(R.string.key_TAB)
            KeyType.ESCAPE -> label = context.resources.getString(R.string.key_ESCAPE)
            KeyType.SUPER -> label = context.resources.getString(R.string.key_SUPER)
            KeyType.ALT -> label = context.resources.getString(R.string.key_ALT)
            KeyType.ALT_GR -> label = context.resources.getString(R.string.key_ALT_GR)
            KeyType.BACKSPACE -> label = context.resources.getString(R.string.key_BACKSPACE)
            KeyType.PAGE_DOWN -> label = context.resources.getString(R.string.key_PAGE_DOWN)
            KeyType.PAGE_UP -> label = context.resources.getString(R.string.key_PAGE_UP)
            KeyType.POS_1 -> label = context.resources.getString(R.string.key_POS_1)
            KeyType.END -> label = context.resources.getString(R.string.key_END)
            KeyType.INSERT -> label = context.resources.getString(R.string.key_INSERT)
            KeyType.DELETE -> label = context.resources.getString(R.string.key_DELETE)
            KeyType.LEFT -> label = context.resources.getString(R.string.key_LEFT)
            KeyType.UP -> label = context.resources.getString(R.string.key_UP)
            KeyType.RIGHT -> label = context.resources.getString(R.string.key_RIGHT)
            KeyType.DOWN -> label = context.resources.getString(R.string.key_DOWN)
            KeyType.F1 -> label = context.resources.getString(R.string.key_F1)
            KeyType.F2 -> label = context.resources.getString(R.string.key_F2)
            KeyType.F3 -> label = context.resources.getString(R.string.key_F3)
            KeyType.F4 -> label = context.resources.getString(R.string.key_F4)
            KeyType.F5 -> label = context.resources.getString(R.string.key_F5)
            KeyType.F6 -> label = context.resources.getString(R.string.key_F6)
            KeyType.F7 -> label = context.resources.getString(R.string.key_F7)
            KeyType.F8 -> label = context.resources.getString(R.string.key_F8)
            KeyType.F9 -> label = context.resources.getString(R.string.key_F9)
            KeyType.F10 -> label = context.resources.getString(R.string.key_F10)
            KeyType.F11 -> label = context.resources.getString(R.string.key_F11)
            KeyType.F12 -> label = context.resources.getString(R.string.key_F12)
        }
    }
}