/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.controls.keyboard

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import pauni.lara.controls.keyboard.KeyState.*
import pauni.lara.core.LinuxMachine
import pauni.lara.core.Prefs
import java.util.*
import kotlin.collections.LinkedHashSet

/**
 * This calls behaves as a virtual keyboard with keys you can press/release.
 * It converts a key press like "A" or "SHIFT" into the command accepted by evemu-events,
 * manages pressed keys and delegates the event to the linuxMachine object.
 */
class Keyboard(context: Context?, private val linuxMachine: LinuxMachine) {
    private val keys: MutableSet<Key> = LinkedHashSet()
    private val pressedKeys: MutableSet<Key> = LinkedHashSet()


    init {
        // create a key for each type
        Arrays.stream(KeyType.values()).forEach { keyType: KeyType ->
            val key = Key(context!!, keyType)
            key.getState().observe(context as LifecycleOwner) {state: KeyState ->
                // call key event on remote machine, mark/un-mark key as pressed
                linuxMachine.execute(keyCommand(getKeyName(key.type), key.getState().value!!))

                if (state == PRESSED) this.pressedKeys.add(key) else this.pressedKeys.remove(key)
            }
            keys.add(key)
        }
    }

    /**
     * Create command to trigger given key name and state.
     * @param keyName Name of the key according to evemu library.
     * @param keyState State of the key.
     */
    private fun keyCommand(keyName: String, keyState: KeyState): String {
       return "sudo evemu-event ${Prefs.loadString(Prefs.KEY_KEYBOARD_PATH)} " +
                    "--type EV_KEY " +
                    "--code KEY_$keyName " +
                    "--value ${if (keyState == PRESSED) "1" else "0"} " +
                    "--sync"
    }

    /**
     * Exposes the key list without mutability.
     * @return List of keys this keyboard contains.
     */
    fun getKeys(): List<Key> {
        return keys.toList()
    }

    /**
     * Press and release a key on the keyboard.
     * @param key String of keys to be pressed and released - e.g. "abc1)" // atm only 1 char
     */
    fun pressAndRelease(key: String) {
        // remote execute press and release command
        linuxMachine.execute(keyCommand(getKeyName(key), PRESSED) +
                " && " + keyCommand(getKeyName(key), RESTING))

        Thread.sleep(50)
        // release all pressed non-char keys, unless marked as sticky
        for (pressedKey in pressedKeys.reversed())
            if (!pressedKey.getSticky()) pressedKey.setState(RESTING)

    }

    /**
     * Convert a key object to the string representation of that key in the evemu library
     * @param key Key object to get the event name from
     * @return String representation of given key in the evemu library
     */
    private fun getKeyName(key: KeyType?): String {
        return when (key) {
            KeyType.CTRL -> "LEFTCTRL"
            KeyType.SHIFT -> "LEFTSHIFT"
            KeyType.TAB -> "TAB"
            KeyType.ESCAPE -> "ESC"
            KeyType.SUPER -> "LEFTMETA"
            KeyType.ALT -> "LEFTALT"
            KeyType.ALT_GR -> "RIGHTALT"
            KeyType.POS_1 -> "HOME"
            KeyType.PAGE_UP -> "PAGEUP"
            KeyType.PAGE_DOWN -> "PAGEDOWN"
            else -> key.toString()
        }
    }

    /**
     * Convert string (currently length 1) to the string representation
     * of that key in the evemu library
     * @param key String to get the event name from
     * @return String representation of given key in the evemu library
     */
    private fun getKeyName(key: String): String {
        return if (!" -=();'`\\,./*".contains(key)) {
            key.uppercase(Locale.getDefault())
        } else when (key) {
            "-" -> "MINUS"
            "=" -> "EQUAL"
            "(" -> "LEFTBRACE"
            ")" -> "RIGHTBRACE"
            ";" -> "SEMICOLON"
            "'" -> "APOSTROPHE"
            "`" -> "GRAVE"
            "\\" -> "BACKSLASH"
            "," -> "COMMA"
            "." -> "DOT"
            "/" -> "SLASH"
            "*" -> "KPASTERISK"
            " " -> "SPACE"
            "\t" -> "TAB"
            else -> ""
        }
    }
}