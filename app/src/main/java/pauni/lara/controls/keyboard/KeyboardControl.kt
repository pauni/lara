/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.controls.keyboard

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.textfield.TextInputEditText
import pauni.lara.R
import pauni.lara.controls.Control
import pauni.lara.core.LinuxMachine
import pauni.lara.core.Prefs

/**
 * Entry point and interface of the keyboard control logic for the rest of the app.
 */
class KeyboardControl(context: Context, linuxMachine: LinuxMachine) : Control(context, linuxMachine) {
    override var controlView: ViewGroup? = null
    var keyboard: Keyboard

    override fun init() {
        // find keyboard, save path to prefs
        val detector = VirtualKeyboardDetector(linuxMachine, context)
        Prefs.saveString(Prefs.KEY_KEYBOARD_PATH, detector.getKeyboardPath())

        controlView = LayoutInflater.from(context).inflate(R.layout.keyboard_control, null) as ViewGroup
        val chgKeyboardKeys: ChipGroup = controlView!!.findViewById(R.id.chg_functionKeys)

        // Create a key for each type and add it to keyboardElems
        val keyboardKeys = keyboard.getKeys()

        // create key view, add to chip group
        for (key in keyboardKeys) {
            val chipKey = Chip(context)
            chipKey.text = key.label
            chipKey.chipIcon = key.icon
            chipKey.setOnClickListener(key)
            chipKey.setOnLongClickListener(key)
            chgKeyboardKeys.addView(chipKey)

            // observe state changes and adjust UI
            key.getState().observe(context as AppCompatActivity) { state: KeyState? ->
                chipKey.isChecked = state == KeyState.PRESSED
            }
        }
    }

    override fun registerListeners() {
        // inflate views, reg text watcher
        val txfKeyboard: TextInputEditText = controlView!!.findViewById(R.id.txf_keyboard)
        txfKeyboard.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.isNotEmpty()) {
                    txfKeyboard.removeTextChangedListener(this)
                    txfKeyboard.text!!.replace(0, s.length, "" + s[s.length - 1])
                    txfKeyboard.addTextChangedListener(this)
                }
            }

            override fun afterTextChanged(s: Editable) {
                if (s.isNotEmpty()) {
                    keyboard.pressAndRelease(s.toString())
                }
            }
        })
    }

    override fun unregisterListeners() {}
    override val displayName: String
        get() = "Keyboard"

    @JvmName("getControlView1")
    fun getControlView(): View? {
        return controlView
    }

    override val displayIcon: Drawable?
        get() = context!!.getDrawable(R.drawable.ic_keyboard)

    init {
        keyboard = Keyboard(context, linuxMachine)
    }
}