package pauni.lara.controls.keyboard

import android.content.Context
import android.util.Log
import pauni.lara.core.Files
import pauni.lara.core.LinuxMachine
import pauni.lara.core.Prefs
import pauni.lara.core.Something
import java.io.InputStreamReader
import java.security.MessageDigest
import kotlin.math.pow

class VirtualKeyboardDetector (private val linuxMachine: LinuxMachine, val context: Context) {

    fun getKeyboardPath(): String? {
        // if keyboard doesn't exist on host, register it
        var path = findKeyboardPath()
        if (path == null) {
            registerVirtualKeyboard()
            path = findKeyboardPath()
        }

        return path
    }

    private fun findKeyboardPath(): String? {
        val cmdFindKeyboard = InputStreamReader(context.assets.open("find-keyboard.sh"))
            .readText()
            .replace("[INSERT PASSWORD]", Prefs.loadString(Prefs.KEY_PASSWORD).toString())

        // return whether keyboard is registered on host
        val process = linuxMachine.execute(cmdFindKeyboard, true)
        val result = process?.readStdout()

        if (result?.contains("LARA Virtual Keyboard") != true) return null

        // search for line of lara keyboard
        var path: String? = null
        result.split("\n").forEach {
            Log.v("detect()", it)
            if (it.contains("LARA Virtual Keyboard", true))
                path = it.substringBefore(":\t")
        }

        Log.v("detect()", "returning path=$path")
        return path
    }


    private fun registerVirtualKeyboard() {
        // put keyboard description file into /tmp dir
        val fp = "/tmp/" + generateRandomString() + "lara-keyboard-description.txt"
        val keyboardDescription = Files.load(Files.KEYBOARD_DESCRIPTION, context)
            .replace("\"", "\\\"")
        linuxMachine.execute("echo \"$keyboardDescription\" > $fp")

        // spawn virtual input device
        val cmdRegisterKeyboard = Files.load(Files.REGISTER_KEYBOARD, context)
            .replace("[INSERT PASSWORD]", Prefs.loadString(Prefs.KEY_PASSWORD).toString())
            .replace("[INSERT PATH]", fp)
        linuxMachine.execute(cmdRegisterKeyboard)

        // give it few cycles to complete
        Thread.sleep(50)

    }

    private fun generateRandomString(): String {
        val random = (Math.random()*100000).pow(Math.random()*1000000)
        return MessageDigest.getInstance("MD5")
            .digest(random.toString().toByteArray())
            .joinToString("") { String.format("%02x", it) }
    }
}