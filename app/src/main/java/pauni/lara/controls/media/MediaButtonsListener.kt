/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.controls.media

import android.content.Context
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.content.res.AppCompatResources
import pauni.lara.R
import pauni.lara.core.LinuxMachine
import pauni.lara.controls.media.RepeatState.*

/**
 * Contains callback methods for various media buttons.
 */
class MediaButtonsListener(private val context: Context, private val linuxMachine: LinuxMachine) : View.OnClickListener {
    private var shuffle = false
    private var repeat = REPEAT_OFF

    /***
     * Handle media button clicks.
     * @param view View the click is performed on
     */
    override fun onClick(view: View) {
        when (view.id) {
            R.id.imb_seek_fw        -> linuxMachine.execute("playerctl position 10+")
            R.id.imb_seek_bw        -> linuxMachine.execute("playerctl position 10-")
            R.id.imb_plause         -> linuxMachine.execute("playerctl play-pause")
            R.id.imb_skip_next      -> linuxMachine.execute("playerctl next")
            R.id.imb_skip_previous  -> linuxMachine.execute("playerctl previous")
            R.id.imb_repeat         -> cycleRepeat(view as ImageButton)
            R.id.imb_shuffle        -> cycleShuffle(view as ImageButton)
        }
    }

    /**
     * Callback method for repeat button, cycling between different repeat modes
     * @param view View that triggered the callback
     */
    private fun cycleRepeat(view: ImageButton) {
        val imgId = when(repeat) {
            REPEAT_OFF -> R.drawable.ic_repeat_off
            REPEAT_ALL -> R.drawable.ic_repeat_all
            REPEAT_ONE -> R.drawable.ic_repeat_one
        }

        view.setImageDrawable(AppCompatResources.getDrawable(context, imgId))
        repeat = repeat.next()
    }

    /**
     * Callback method for shuffle button, cycling between different shuffle modes
     * @param view View that triggered the callback
     */
    private fun cycleShuffle(view: ImageButton) {
        shuffle = !shuffle
        val imgId = if (shuffle) R.drawable.ic_shuffle_on else R.drawable.ic_shuffle
        val cmdShuffle = "playerctl shuffle " +  if (shuffle) "on" else "off"

        view.setImageDrawable(AppCompatResources.getDrawable(context, imgId))
        linuxMachine.execute(cmdShuffle)
    }
}