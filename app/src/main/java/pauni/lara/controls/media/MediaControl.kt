/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.controls.media

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import com.google.android.material.slider.Slider
import pauni.lara.R
import pauni.lara.controls.Control
import pauni.lara.core.LinuxMachine

/**
 * Entry point and interface of the media control logic for the rest of the app.
 */
class MediaControl(context: Context, linuxMachine: LinuxMachine) : Control(context, linuxMachine) {
    override var controlView: View? = null
        private set
    private val ibmPlause: ImageButton? = null
    private val ibmSeekFw: ImageButton? = null
    private val ibmSeekBw: ImageButton? = null
    private val imbSkipNext: ImageButton? = null
    private val imbSkinPrev: ImageButton? = null
    private val imbMute: ImageButton? = null
    private val sliVolume: Slider? = null

    /**
     * I won't write a description for this method
     * @return No, won't do it
     */
    var isMuted = false
        private set

    override fun init() {
        controlView = LayoutInflater.from(context).inflate(R.layout.media_control, null)
    }

    override fun registerListeners() {
        val mediaButtonListener = MediaButtonsListener(context!!, linuxMachine!!)

        // TODO: implement support for visually impaired users
        controlView!!.findViewById<View>(R.id.imb_plause).setOnClickListener(mediaButtonListener)
        controlView!!.findViewById<View>(R.id.imb_seek_fw).setOnClickListener(mediaButtonListener)
        controlView!!.findViewById<View>(R.id.imb_seek_bw).setOnClickListener(mediaButtonListener)
        controlView!!.findViewById<View>(R.id.imb_skip_next).setOnClickListener(mediaButtonListener)
        controlView!!.findViewById<View>(R.id.imb_skip_previous).setOnClickListener(mediaButtonListener)
        controlView!!.findViewById<View>(R.id.imb_repeat).setOnClickListener(mediaButtonListener)
        controlView!!.findViewById<View>(R.id.imb_shuffle).setOnClickListener(mediaButtonListener)
        controlView!!.findViewById<View>(R.id.imb_mute).setOnClickListener(MuteButtonListener(context!!, this))
        (controlView!!.findViewById<View>(R.id.sb_volume) as Slider).addOnChangeListener(OnVolumeChangeListener(this))
    }

    override fun unregisterListeners() {
        ibmPlause!!.setOnClickListener(null)
        ibmSeekFw!!.setOnClickListener(null)
        ibmSeekBw!!.setOnClickListener(null)
        sliVolume!!.clearOnChangeListeners()
    }

    override val displayName: String
        get() = "Playback"

    override val displayIcon: Drawable?
        get() = context.getDrawable(R.drawable.ic_play)

    /**
     * Mutes the Linux machine's volume.
     */
    fun volumeToggleMute() {
        isMuted = !isMuted
        linuxMachine.execute("amixer -D pulse sset Master ${if (!isMuted) "un" else "" }mute")
    }

    /**
     * Sets Linux machine's volume to the specified 'vol'.
     * @param vol The new value for the system volume.
     */
    fun setVolume(vol: Int) {
        linuxMachine.execute("amixer -D pulse sset Master $vol%")
    }

}