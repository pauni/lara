/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.controls.media

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.ImageButton
import pauni.lara.R

/**
 * OnClickListener for mute-toggle-button in the volume control.
 * Updates button drawable and executes command on the Linux machine.
 */
@SuppressLint("UseCompatLoadingForDrawables")
class MuteButtonListener(private val context: Context, private val mediaControl: MediaControl) : View.OnClickListener {
    override fun onClick(v: View) {
        // TODO: confirm action was successful
        mediaControl.volumeToggleMute()
        (v as ImageButton).setImageDrawable(context.resources.getDrawable(
                if (mediaControl.isMuted) R.drawable.ic_volume_off else R.drawable.ic_volume_on))
    }

}