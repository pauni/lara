/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.controls.media

import com.google.android.material.slider.Slider

/**
 * VolumeChangeListener for the volume slide in the media control.
 */
class OnVolumeChangeListener(var control: MediaControl) : Slider.OnChangeListener {
    override fun onValueChange(slider: Slider, value: Float, fromUser: Boolean) {
        control.setVolume(Math.round(value))
    }

}