package pauni.lara.controls.media

enum class RepeatState {
    REPEAT_OFF,
    REPEAT_ALL,
    REPEAT_ONE;

    fun next(): RepeatState {
        val size = values().size
        return values()[(this.ordinal + 1) % size]
    }
}