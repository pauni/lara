package pauni.lara.core

interface EventReceiver<T> {
    fun onEvent(type: String, data: T?)
}