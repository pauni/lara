package pauni.lara.core

import android.content.Context
import java.io.InputStreamReader

class Files {
    companion object {
        const val REGISTER_KEYBOARD = "register-keyboard.sh"
        const val KEYBOARD_DESCRIPTION = "keyboard-description.txt"
        const val AS_SUDO = "as-sudo.sh"

        fun load(filename: String, context: Context): String {
            return InputStreamReader(context.assets.open(filename)).readText()
        }
    }
}