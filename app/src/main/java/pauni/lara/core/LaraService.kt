/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.core

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import pauni.lara.core.LaraService

/**
 * Background service for SSH connection etc.
 */
class LaraService : Service() {
    private val binder: IBinder = LocalBinder()
    override fun onBind(intent: Intent): IBinder? {
        return binder
    }

    /**
     * Class used for the client Binder. Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        // return this instance of LocalService so clients can call public methods
        val service: LaraService
            get() =// return this instance of LocalService so clients can call public methods
                this@LaraService
    }
}