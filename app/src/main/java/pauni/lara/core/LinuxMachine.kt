/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.core

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log

/**
 * Representing a Linux machine with SSH (and various additional packages
 * required by this app) installed.
 * Can be used to execute [Something] on the connected Linux Machine
 */
class LinuxMachine(private val context: Context) : EventReceiver<Any> {

    private val handler = Handler(Looper.getMainLooper())
    private var ssh: SSHelper? = null


    init {
        ssh = createSSHSession()
        // TODO: put network in bg service, then call here: ssh?.connect()
        disconnectInSeconds(60)
    }

    /**
     * Executes 'command' via an SSH connection on remote Linux machine.
     * @param command Command to be executed on the remote Linux machine.
     */
    fun execute(command: String, blocking: Boolean = false): Process? {
        // if session is not initialized yet or disconnected, create new one
        if (ssh == null || ssh?.isConnected() == false) {
            Log.i("execute()", "No session, creating new one")
            ssh = createSSHSession()

            // TODO: put network in bg service, then call here:  ssh?.connect()
        }

        val process = ssh?.exec(command)
        disconnectInSeconds(60)

        // if blocking flag is set, wait until process finishes to return
        if (blocking)
            while (process?.isFinished() != true)
                Thread.sleep(10)

        return process
    }

    /**
     * Establishes and returns a SSH session with remote Linux machine.
     */
    private fun createSSHSession() : SSHelper? {
        try {
            val user = Prefs.loadString(Prefs.KEY_USER)
            val password = Prefs.loadString(Prefs.KEY_PASSWORD)
            val host = Prefs.loadString(Prefs.KEY_HOST)
            val port = Prefs.loadInt(Prefs.KEY_PORT)
            if (port < 1 || host!!.isEmpty() || user!!.isEmpty()) {
                // TODO: display snack error
                return null
            }

            return SSHelper(user, password, host, port)
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    /**
     * Disconnects from the Linux machine after 10 seconds.
     * Prevents unnecessarily many connection establishments during usage.
     */
    private fun disconnectInSeconds(seconds: Int) {
        // remove all previous callbacks so we don't disconnect twice
        handler.removeCallbacksAndMessages(null)

        // issue disconnection (after 1min. TODO: figure out best value
        handler.postDelayed({ssh?.disconnect()}, seconds * 1000.toLong())
    }

    private fun disconnectNow() {
        ssh?.disconnect()
    }

    /**
     * Event callback for when the app is being closed (killed).
     * Attempting to close open connection.
     */
    override fun onEvent(type: String, data: Any?) {
        disconnectNow()
    }
}