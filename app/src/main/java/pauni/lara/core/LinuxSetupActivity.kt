package pauni.lara.core

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import pauni.lara.databinding.ActivitySetupBinding
import java.io.InputStreamReader
import java.security.MessageDigest
import kotlin.math.pow

/**
 * This class takes care of adding/setting up a remote Linux computer.
 * It asks the user for user:password information and uses them to create
 * a new user 'lara-remote' which will be used subsequently by this app.
 */
class LinuxSetupActivity: AppCompatActivity() {
    private lateinit var binding: ActivitySetupBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySetupBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.swcAdvancedSettings.setOnCheckedChangeListener { _, b ->
            binding.linAdvancedSettings.visibility = if (b) View.VISIBLE else View.GONE
        }

        binding.btnSave.setOnClickListener {
//            createUserOnHost(Constants.APP_USER_NAME, generatePassword())
            Prefs.saveString(Prefs.KEY_USER, binding.txiUser.text.toString())
            Prefs.saveString(Prefs.KEY_PASSWORD, binding.txiPassword.text.toString())
            Prefs.saveString(Prefs.KEY_HOST, binding.txiHost.text.toString())
            Prefs.saveInt(Prefs.KEY_PORT, binding.txiPort.text.toString().toInt())
            finish()
        }

        binding.txiUser.setText(Prefs.loadString(Prefs.KEY_USER))
        binding.txiPassword.setText(Prefs.loadString(Prefs.KEY_PASSWORD))
        binding.txiHost.setText(Prefs.loadString(Prefs.KEY_HOST))
        binding.txiPort.setText(Prefs.loadInt(Prefs.KEY_PORT).toString())
    }

    /**
     * Creates a user called 'lara-remote' with a randomly generated password
     * and adds 'lara-remote' to sudoers using user provided
     */
    private fun createUserOnHost(username: String, password: String) {
        // for the replace calls just take a look
        // creates user 'lara-remote'
        val commandCreateUser = InputStreamReader(assets.open("create-user.sh"))
            .readText()
            .replace("[INSERT USERNAME]", username)
            .replace("[INSERT PASSWORD]", password)
            .replace("[INSERT SUDO PASSWORD]", binding.txiPassword.text.toString())

        // adds newly created user 'lara-remote' to sudoers
        val commandAddToSudoers = InputStreamReader(assets.open("make-sudoer.sh"))
            .readText()
            .replace("[INSERT USERNAME]", username)
            .replace("[INSERT SUDO PASSWORD]", binding.txiPassword.text.toString())

        // connect to the machine the user just entered and create user for this app
        val ssh = SSHelper(
            binding.txiUser.text.toString(),
            binding.txiPassword.text.toString(),
            binding.txiHost.text.toString(),
            binding.txiPort.text.toString().toInt()
        )
    }

    private fun generatePassword(): String {
        val random = (Math.random()*100000).pow(Math.random()*1000000)
        return MessageDigest.getInstance("MD5")
            .digest(random.toString().toByteArray())
            .joinToString("") { String.format("%02x", it) }
    }

    fun dismiss(v: View) {
        finish()
    }
}