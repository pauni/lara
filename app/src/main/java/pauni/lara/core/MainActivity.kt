/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.core

import android.annotation.SuppressLint
import android.content.*
import android.graphics.Typeface
import android.os.Bundle
import android.os.IBinder
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.PopupMenu
import pauni.lara.LaraController
import pauni.lara.R
import pauni.lara.controls.Control
import pauni.lara.controls.ControlView
import pauni.lara.controls.keyboard.KeyboardControl
import pauni.lara.controls.media.MediaControl
import java.io.InputStreamReader
import kotlin.collections.HashSet

class MainActivity : AppCompatActivity(), PopupMenu.OnMenuItemClickListener {
    var laraController: LaraController? = null
        private set
    private var isBound = false
    private val killListeners: HashSet<EventReceiver<Any>> = HashSet()

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        setContentView(R.layout.activity_main)
        Prefs.init(this)

        val user = Prefs.loadString(Prefs.KEY_USER)
        val host = Prefs.loadString(Prefs.KEY_HOST)
        val password = Prefs.loadString(Prefs.KEY_PASSWORD)
        val port = Prefs.loadInt(Prefs.KEY_PORT)
        if (port == -1 || host!!.isEmpty() || user!!.isEmpty() || password!!.isEmpty()) {
            val intent = Intent(this, LinuxSetupActivity::class.java)
            startActivity(intent)
        } else {
            loadControls()
        }

    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.option_add_computer -> {
                startActivity(Intent(this, LinuxSetupActivity::class.java))
                true
            }
            R.id.option_licences -> {
                val msg = InputStreamReader(assets.open("licenses.txt")).readText()
                val dialog = AlertDialog.Builder(this).apply {
                    setTitle("Licenses")
                    setMessage("Notice for JSch:\n\n$msg")
                    setPositiveButton(android.R.string.ok)
                        { dialogInterface, _ -> dialogInterface.dismiss() }
                }.show()
                val txtMessage = dialog.findViewById<TextView>(android.R.id.message)
                txtMessage?.typeface = Typeface.MONOSPACE
                true
            }
            else -> false
        }
    }

    fun showSettingsMenu(v: View) {
        PopupMenu(this, v).apply {
            setOnMenuItemClickListener (this@MainActivity)
            inflate(R.menu.main_menu)
            show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        notifyKillListeners()
    }

    private fun notifyKillListeners() {
        killListeners.forEach { it.onEvent("kill", null) }
    }

    private val connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            laraController = (service as LaraController.LocalBinder).service
            isBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            // unexpected closing of the service
            isBound = false
        }
    }

    private fun loadControls() {
        val linuxMachine = LinuxMachine(this)
        val llControls = findViewById<LinearLayout>(R.id.ll_controls)
        val controls: MutableSet<Control> = HashSet()
        killListeners.add(linuxMachine)

        controls.add(KeyboardControl(this, linuxMachine))
        controls.add(MediaControl(this, linuxMachine))
        // ...
        for (control in controls) {
            control.init()
            control.registerListeners()
            llControls.addView(ControlView(this, control))
        }
    }

    private fun globalToast(msg: String) {
        runOnUiThread { Toast.makeText(this@MainActivity, msg, Toast.LENGTH_SHORT).show() }
    }

    fun onShutdownClick(view: View?) {
        AlertDialog.Builder(this)
                .setPositiveButton(resources.getString(android.R.string.yes)) { dialog: DialogInterface?, which: Int -> }
                .setTitle("Shutdown")
                .setMessage("Are you sure you want to shutdown your computer?")
                .setCancelable(true)
                .show()
    }

    fun onUserHelpClick(view: View?) {
        showInfoDialog("Username", "Enter your user name you use to login to your computer.")
    }

    fun onHostHelpClick(view: View?) {
        showInfoDialog("Hostname", "Enter either your hostname or IP address of" +
                " your computer. The hostname is a label that is assigned to a device connected to" +
                " a computer network that is used to identify the device.")
    }

    fun onPasswordHelpClick(view: View?) {
        showInfoDialog("Password", "Enter your username you use to login to your computer")
    }

    fun onPortHelpClick(view: View?) {
        showInfoDialog("Port", "The standard TCP port for SSH is 22." +
                "If your SSH server is listening on a different port you have to specify that here")
    }

    fun showInfoDialog(title: String?, infoMessage: String?) {
        AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(infoMessage)
                .setPositiveButton(resources.getString(android.R.string.ok)) { dialog, which -> dialog.cancel() }
                .show()
    }
}