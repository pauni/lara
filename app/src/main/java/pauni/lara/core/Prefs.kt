/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.core

import android.content.Context
import android.content.SharedPreferences

/**
 * SharedPreferences helper class
 */
object Prefs {
    const val KEY_USER = "user"
    const val KEY_HOST = "host"
    const val KEY_PASSWORD = "password"
    const val KEY_PORT = "port"
    const val KEY_KEYBOARD_PATH = "keyboard_path"
    const val KEY_STEP = "step_size"
    private const val USER_PREF = "user_prefs"
    private var preferences: SharedPreferences? = null

    fun loadString(key: String?): String? {
        return preferences!!.getString(key, "")
    }

    fun init(context: Context) {
        preferences = context.getSharedPreferences(USER_PREF, Context.MODE_PRIVATE)
        if (loadInt(KEY_PORT) == -1) {
            val prefs = preferences
            prefs?.edit()?.putInt(KEY_PORT, 22)?.apply()
        }
    }

    fun loadInt(key: String?): Int {
        return preferences!!.getInt(key, -1)
    }

    fun saveInt(key: String?, `val`: Int) {
        preferences!!.edit().putInt(key, `val`).apply()
    }

    fun saveString(key: String?, `val`: String?) {
        preferences!!.edit().putString(key, `val`).apply()
    }
}