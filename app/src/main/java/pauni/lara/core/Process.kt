package pauni.lara.core

import android.os.Handler
import android.os.Looper
import android.util.Log
import com.jcraft.jsch.ChannelExec
import java.io.ByteArrayOutputStream

class Process(private val stdout: ByteArrayOutputStream) {
    private var finished: Boolean = false
    private var channel: ChannelExec? = null
    private var onFinishCallback: (Process) -> Unit = {}
    private val handler = Handler(Looper.getMainLooper())

    class Signal {
        companion object {
            const val SIGHUP      = 1
            const val SIGINT      = 2
            const val SIGQUIT     = 3
            const val SIGILL      = 4
            const val SIGTRAP     = 5
            const val SIGABRT     = 6
            const val SIGBUS      = 7
            const val SIGFPE      = 8
            const val SIGKILL     = 9
            const val SIGUSR1     = 10
            const val SIGSEGV     = 11
            const val SIGUSR2     = 12
            const val SIGPIPE     = 13
            const val SIGALRM     = 14
            const val SIGTERM     = 15
            const val SIGSTKFLT   = 16
            const val SIGCHLD     = 17
            const val SIGCONT     = 18
            const val SIGSTOP     = 19
            const val SIGTSTP     = 20
            const val SIGTTIN     = 21
            const val SIGTTOU     = 22
            const val SIGURG      = 23
            const val SIGXCPU     = 24
            const val SIGXFSZ     = 25
            const val SIGVTALRM   = 26
            const val SIGPROF     = 27
            const val SIGWINCH    = 28
            const val SIGIO       = 29
            const val SIGPWR      = 30
            const val SIGUNUSED   = 31
        }
    }

    fun isFinished(): Boolean {
        return finished
    }

    fun onFinish() {
        finished = true
        onFinishCallback(this)

        Log.v("Process", "onFinish(). Result=" + this.readStdout())
    }

    /**
     * Add callback for onFinish. If already finished, callback gets called immediately.
     * @param callback Callback with no parameters.
     */
    fun setOnFinishListener(callback: (Process) -> Unit): Process {
        if (finished) callback(this)
        onFinishCallback = callback

        return this
    }

    /**
     * Returns what the process has written so far to stdout.
     */
    fun readStdout(): String {
        return stdout.toString()
    }

    /**
     * Send a signal to the process like SIGTERM or SIGINT.
     * @param signal The signal to be sent to the process - see inner class [Signal].
     */
    fun sendSig(signal: Int) {
        channel?.sendSignal(signal.toString())
    }

    fun setChannel(channel: ChannelExec?) {
        this.channel = channel
    }

    fun setTimeout(milliseconds: Long) {
        handler.postDelayed({ Thread {this.sendSig(Signal.SIGINT)}.start() }, milliseconds)
    }
}
