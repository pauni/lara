package pauni.lara.core
import android.util.Log
import com.jcraft.jsch.*
import java.io.ByteArrayOutputStream
import java.lang.NullPointerException
import kotlin.Exception

class SSHelper(
    private val user: String, val password: String?,
    val host: String, val port: Int) {
    private var session: Session? = null

    init {
        openSession()
    }

    private fun openSession() {
        val jsch = JSch()
        session = jsch.getSession(user, host, port)
        session?.setPassword(password) // TODO: check if doc is correct, pw==null is okay?
        session?.setConfig("StrictHostKeyChecking", "no")
    }


    private fun connect() : Boolean {
        return try {
            if (session == null || session?.isConnected == false) openSession()
            session?.connect()
            session?.isConnected!!
        } catch (e: Exception) {
            session?.disconnect()
            false
        }
    }

    fun exec(command: String): Process {
        var channel: ChannelExec? = null
        val responseStream = ByteArrayOutputStream()
        val process = Process(responseStream)

        Thread {
            Thread.sleep(20)
            if (session == null || session?.isConnected == false) connect()
            try {
                channel = session?.openChannel("exec") as ChannelExec }
            catch (e: Exception) { Log.e("SSHelper", "TODO: Show error to user", e) }
            process.setChannel(channel)

            try {
                // creating channel, init out streams
                val errorStream = ByteArrayOutputStream()
                channel?.setCommand(command)
                channel?.outputStream = responseStream
                channel?.setErrStream(errorStream)

                // execute command and wait for reply
                channel?.connect()
                while (channel?.isConnected == true) {
                    Thread.sleep(5)
                }


                val errorResponse = errorStream.toString()
                if (errorResponse.isNotEmpty()) throw java.lang.Exception(errorResponse)
                channel?.disconnect()
                process.onFinish()
            } catch (e: Exception) {
                Log.e("SSHelper", "TODO: Display error message to user", e)
            }
        }.start()

        return process
    }

    fun isConnected(): Boolean {
        return session != null && session?.isConnected == true
    }

    fun disconnect() {
        Log.i("SSHelper", "disconnecting SSH session")
        session?.disconnect()
    }

}