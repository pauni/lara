/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.dialog

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.View.OnAttachStateChangeListener
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import pauni.lara.R
import pauni.lara.animation.Animations

/**
 * Custom dialog, prettier than defaults (imo at least)
 */
open class FancyDialog(protected val anchor: View?, context: Context?) : Dialog(context!!, R.style.FancyDialogTheme) {
    // protected val context: Context?
    protected val dialogRoot: View? = this.findViewById(R.id.dialog_root)
    private val txtTitle: TextView? = this.findViewById(R.id.txt_dialog_title)
    private val txtMessage: TextView? = this.findViewById(R.id.txt_dialog_message)
    private val frmCustom: FrameLayout? = this.findViewById(R.id.frm_custom)
    private val btnPositive: Button? = this.findViewById(R.id.btn_positive)
    private val btnNeutral: Button? = this.findViewById(R.id.btn_neutral)
    private val btnNegative: Button? = this.findViewById(R.id.btn_negative)
    private var hasAnchor: Boolean = anchor != null
    private val model: FancyDialogViewModel
    private var animationOrigin // view that "causes" the animation (visually)
            : View? = null
    private var x = 0
    private var y = 0
    private var enterCompleteListener: Runnable? = null


    /**
     * Creates an instance of the dialog, set above the anchorView, which will
     * be fully obscured by the dialog view. If no anchorView is set, the dialog will
     * be floating in the middle of the screen, just as a normal AlertDialog.
     * @param anchor The view that this dialog should obscure
     * @param context Context of the app.
     */
    init {
        model = ViewModelProvider(context as ViewModelStoreOwner).get(FancyDialogViewModel::class.java)
        this.setOnShowListener { // measure preferred center of animation
            if (animationOrigin != null) {
                measureAnimationCenter()
            }

            // reveal by circular animation
            val revealAnimation = Animations.circularReveal(dialogRoot, x, y)
            revealAnimation.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    if (enterCompleteListener != null) enterCompleteListener!!.run()
                }
            })

            // start animation
            revealAnimation.start()
        }
    }

    /**
     * Same as above but without an anchor.
     * @param context Context of the app.
     */
    constructor(context: Context?) : this(null, context) {}

    private fun measureAnimationCenter() {
        val dialogLocation = IntArray(2)
        val animOriginLocation = IntArray(2)
        val dx: Int
        val dy: Int

        // measure absolute location of dialog and anim origin
        dialogRoot!!.getLocationOnScreen(dialogLocation)
        animationOrigin!!.getLocationOnScreen(animOriginLocation)

        // calculate relative position of origin to dialog
        dx = Math.abs(dialogLocation[0] - animOriginLocation[0])
        dy = Math.abs(dialogLocation[1] - animOriginLocation[1])

        // calculate actual center of the view, by adding half of it's dimensions
        x = dx + animationOrigin!!.measuredWidth / 2
        y = dy + animationOrigin!!.measuredHeight / 2
    }

    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fancy_dialog)
        initViews()
        bindViewsToData()

        // animation and dialog positioning stuff
        dialogRoot!!.addOnAttachStateChangeListener(object : OnAttachStateChangeListener {
            override fun onViewDetachedFromWindow(v: View) {}
            override fun onViewAttachedToWindow(v: View) {
                // hide dialog layout for the animated reveal
                dialogRoot!!.visibility = View.INVISIBLE
                dialogRoot!!.requestLayout()

                // measure the dialog width for the animated reveal
                dialogRoot!!.measure(-1, -1)
                val window = window
                        ?: return  // fck...
                val wlp = window.attributes
                if (hasAnchor) {
                    // apply anchor dimensions to this window
                    window.setLayout(anchor!!.width, anchor.height)

                    // get absolute coordinates of anchor view
                    val location = IntArray(2)
                    anchor.getLocationOnScreen(location)
                    wlp.gravity = Gravity.TOP or Gravity.START
                    wlp.x = location[0]
                    wlp.y = location[1]
                } else {
                    // adjust to be a "floating" dialog
                    wlp.gravity = Gravity.CENTER
                    window.setDimAmount(0.1f)
                    window.setBackgroundDrawable(InsetDrawable(
                            ColorDrawable(Color.TRANSPARENT), 40))
                    window.setLayout(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT)
                    dialogRoot!!.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
                }
                dialogRoot!!.requestLayout()
                window.attributes = wlp
            }
        })
    }

    private fun bindViewsToData() {
        model.getTitle().observe((context as LifecycleOwner?)!!, Observer { newTitle: String? -> txtTitle!!.text = newTitle })
        model.getMessage().observe((context as LifecycleOwner?)!!, Observer { newMessage: String? ->
            // Update the UI, in this case, a TextView.
            txtMessage!!.text = newMessage
        })
        model.getBtnPosText().observe((context as LifecycleOwner?)!!, Observer { newText: String? ->
            // update button text and make it visible
            btnPositive!!.text = newText
            btnPositive!!.visibility = View.VISIBLE
        })
        model.getBtnNeuText().observe((context as LifecycleOwner?)!!, Observer { newText: String? ->
            // update button text and make it visible
            btnNeutral!!.text = newText
            btnNeutral!!.visibility = View.VISIBLE
        })
        model.getBtnNegText().observe((context as LifecycleOwner?)!!, Observer { newText: String? ->
            // update button text and make it visible
            btnNegative!!.text = newText
            btnNegative!!.visibility = View.VISIBLE
        })
        model.getCustomView().observe((context as LifecycleOwner?)!!, Observer { child: View? ->
            // clear previous view (if existent)
            frmCustom!!.removeAllViews()
            frmCustom!!.addView(child)
        })
        btnPositive!!.setOnClickListener { v: View? ->
            val clickListener = model.getBtnPosListener().value
            if (clickListener != null) clickListener.onClick(v) else dismiss()
        }
        btnNeutral!!.setOnClickListener { v: View? ->
            val clickListener = model.getBtnNeuListener().value
            if (clickListener != null) clickListener.onClick(v) else dismiss()
        }
        btnNegative!!.setOnClickListener { v: View? ->
            val clickListener = model.getBtnNegListener().value
            if (clickListener != null) clickListener.onClick(v) else dismiss()
        }
    }

    protected fun onEnterAnimationComplete(runnable: Runnable?) {
        enterCompleteListener = runnable
    }

    /**
     * Initializes all the necessary views. Must be called AFTER show()
     */
    private fun initViews() {
        // by default, disable buttons. Re-enabled by LiveData observers
        btnPositive?.visibility = View.GONE
        btnNeutral?.visibility = View.GONE
        btnNegative?.visibility = View.GONE
    }

    /**
     * Sets the title of the dialog.
     * @param title Title of the dialog.
     */
    fun setTitle(title: String?): FancyDialog {
        dialogRoot!!.findViewById<View>(R.id.txt_dialog_title).visibility = if (title == null) View.GONE else View.VISIBLE
        model.setTitle(title)
        return this
    }

    /**
     * Sets the message of the dialog.
     * @param message Message of the dialog.
     */
    fun setMessage(message: String?): FancyDialog {
        dialogRoot!!.findViewById<View>(R.id.txt_dialog_message).visibility = if (message == null) View.GONE else View.VISIBLE
        model.setMessage(message)
        return this
    }

    /**
     * Places a custom view into the dialog.
     * @param view Custom view to be shown in the dialog.
     */
    fun setCustomView(view: View?): FancyDialog {
        model.setCustomView(view)
        return this
    }

    /**
     * Creates a button whose attributes are determined by the buttonType.
     * @param buttonType Must be one of the following values:
     * Dialog.BUTTON_POSITIVE,
     * Dialog.BUTTON_NEUTRAL,
     * Dialog.BUTTON_NEGATIVE.
     * @param text Text of the button.
     * @param listener Click listener for this button.
     */
    fun setButton(buttonType: Int, text: String?, listener: View.OnClickListener?): FancyDialog {
        when (buttonType) {
            DialogInterface.BUTTON_POSITIVE -> {
                model.setBtnPosText(text)
                model.setBtnPosListener(listener)
            }
            DialogInterface.BUTTON_NEUTRAL -> {
                model.setBtnNeuText(text)
                model.setBtnNeuListener(listener)
            }
            DialogInterface.BUTTON_NEGATIVE -> {
                model.setBtnNegText(text)
                model.setBtnNegListener(listener)
            }
            else -> Log.e(javaClass.getName(), "Invalid button type was passed")
        }
        return this
    }

    fun setAnimationOrigin(animationOrigin: View?) {
        this.animationOrigin = animationOrigin
    }

    fun setAnimationOrigin(position: Int): FancyDialog {
        if (position == Gravity.CENTER) {
            animationOrigin = window!!.decorView.findViewById(android.R.id.content)
        }
        return this
    }

    override fun show() {
        super.show()
    }

    override fun dismiss() {
//        Animator animator = Animations.circularHide(dialogRoot, 0, 0);
        super.dismiss()
        model.clearAllValues()
    }
}