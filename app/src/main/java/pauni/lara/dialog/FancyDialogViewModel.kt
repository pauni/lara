/*
This file is part of Lara.

Lara is free software: you can redistribute it and/or modify it under the
terms of the  GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Lara is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lara. If not, see <https://www.gnu.org/licenses/>.
*/
package pauni.lara.dialog

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * View model for content of custom dialogs.
 */

class FancyDialogViewModel : ViewModel() {
    private val title: MutableLiveData<String> by lazy { MutableLiveData<String>()}
    private val message: MutableLiveData<String> by lazy { MutableLiveData<String>()}
    private val btnPosText: MutableLiveData<String> by lazy { MutableLiveData<String>()}
    private val btnNeuText: MutableLiveData<String> by lazy { MutableLiveData<String>()}
    private val btnNegText: MutableLiveData<String> by lazy { MutableLiveData<String>()}
    private val customView: MutableLiveData<View> by lazy { MutableLiveData<View>()}
    private val btnPosListener: MutableLiveData<View.OnClickListener> by lazy { MutableLiveData<View.OnClickListener>()}
    private val btnNeuListener: MutableLiveData<View.OnClickListener> by lazy { MutableLiveData<View.OnClickListener>()}
    private val btnNegListener: MutableLiveData<View.OnClickListener> by lazy { MutableLiveData<View.OnClickListener>()}


    fun getTitle(): LiveData<String?> {
        return title
    }

    fun setTitle(title: String?) {
        this.title.value = title
    }


    fun getMessage(): LiveData<String?> {
        return message
    }

    fun setMessage(message: String?) {
        this.message.value = message
    }


    fun getBtnPosText(): LiveData<String?> {
        return btnPosText
    }

    fun setBtnPosText(text: String?) {
        this.btnPosText.value = text
    }


    fun getBtnNeuText(): LiveData<String?> {
        return btnNeuText
    }

    fun setBtnNeuText(text: String?) {
        this.btnNeuText.value = text
    }


    fun getBtnNegText(): LiveData<String?> {
        return btnNegText
    }

    fun setBtnNegText(text: String?) {
        this.btnNegText.value = text
    }


    fun getCustomView(): LiveData<View?> {
        return customView
    }

    fun setCustomView(view: View?) {
        this.customView.value = view
    }


    fun getBtnPosListener(): LiveData<View.OnClickListener?> {
        return btnPosListener
    }

    fun setBtnPosListener(listener: View.OnClickListener?) {
        this.btnPosListener.value = listener
    }


    fun getBtnNeuListener(): LiveData<View.OnClickListener?> {
        return btnNeuListener
    }

    fun setBtnNeuListener(listener: View.OnClickListener?) {
        this.btnNeuListener.value = listener
    }


    fun getBtnNegListener(): LiveData<View.OnClickListener?> {
        return btnNegListener
    }

    fun setBtnNegListener(listener: View.OnClickListener?) {
        this.btnNegListener.value = listener
    }

    // Unsure whether this is a correct usage of LiveData inside Dialogs...
    fun clearAllValues() {
        this.title.value = null
        this.message.value = null
        this.btnPosText.value = null
        this.btnNeuText.value = null
        this.btnNegText.value = null
        this.customView.value = null
        this.btnPosListener.value = null
        this.btnNeuListener.value = null
        this.btnNegListener.value = null
    }

}